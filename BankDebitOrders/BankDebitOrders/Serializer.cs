﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace BankDebitOrders
{
    public class Serializer
    {
        //converts xml to object
        public T Deserialize<T>(string xml) where T: class
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T), new XmlRootAttribute("debitorders"));
            using (StringReader sr = new StringReader(xml)) {
                return (T)serializer.Deserialize(sr);
            }
        }
    }
}
