﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BankDebitOrders
{
    public class Bank
    {
        public string BankName { get; set; }

        public int RecordCount { get; set; }

        public double Value { get; set; }

        public Bank(string BankName)
        {
            this.BankName = BankName;
            this.RecordCount = 0;
            this.Value = 0;
        }

        public Bank() { }

        public void AddRecord(double Value)
        {
            this.RecordCount++;
            this.Value += Value;
        }
        



    }
}
