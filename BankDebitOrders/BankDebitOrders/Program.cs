﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;


namespace BankDebitOrders
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("processing Bank files...Please wait");
            Serializer ser = new Serializer();
            string path = string.Empty;
            string xmlInputData = string.Empty;
            string xmlOutputData = string.Empty;

            path = Directory.GetCurrentDirectory()+ @"\DebitOrders.xml";
            xmlInputData = File.ReadAllText(path);//read all text from xml file then close file

            DebitOrders debitOrders = ser.Deserialize<DebitOrders>(xmlInputData);
            
            //select all banks to List 
            List<Bank> banks = debitOrders.oDeductions.Select(x => x.BankName).Distinct().Select(x => new Bank { BankName = x }).ToList();

            foreach (var bank in banks)
            {
                StreamWriter writer = new StreamWriter(@"" + bank.BankName + ".txt");

             
                List<Deduction> deductions = debitOrders.oDeductions.Where(x => x.BankName.ToUpper() == bank.BankName.ToUpper()).OrderBy(x => x.Amount).ThenBy(y => y.AccountHolder).ToList();
                //deductions  = deductions.OrderBy(x=>x.Amount).ThenBy(y=>y.AccountHolder)
                double Total = debitOrders.oDeductions.Where(x => x.BankName.ToUpper() == bank.BankName.ToUpper()).Sum(x => x.Amount);
                int recordcount = debitOrders.oDeductions.Where(x=> x.BankName.ToUpper() == bank.BankName.ToUpper()).Count();

                //print header
                string header = bank.BankName.PadRight(16, ' ') +Convert.ToString(recordcount).PadLeft(3,'0')+ Convert.ToString(Total).Replace(".","").Replace(",","").PadLeft(10,'0');
                writer.WriteLine(header);
                writer.WriteLine();

                int count = 0;
                    foreach (var item in deductions)
                    {
                    string[] tempArray = item.AccountHolder.Split(' ');
                    string AccountHolder = tempArray[0].Substring(0, 1) + tempArray[1];
                    string accountType = "";
                    string[] date = item.Date.Split('/');
                    string dateString = date[1] + "/" + date[2] + "/" + date[0];
                    switch (item.AccountType.ToLower())
                    {
                        case "cheque":
                            accountType = "CH";
                            break;
                        case "savings":
                            accountType = "SAV";
                            break;
                        case "credit":
                            accountType = "CR";
                            break;
                        default:
                            accountType = "OTH";
                            break;
                    }
                    string detailRec = AccountHolder.PadRight(15, ' ') + item.AccountNumber.PadRight(14, ' ') + accountType.PadRight(3, ' ')  + item.Branch.PadRight(10, ' ') + Convert.ToString(item.Amount).Replace(".", "").Replace(",","").PadLeft(7, '0') + dateString.Replace("/", "");
                    writer.WriteLine(detailRec);
                        count++;
                    }
                //end file with carriage return
                string CRLF = "\n";
                writer.WriteLine(CRLF.Replace("\r", "\n"));

                writer.Close();
                Console.WriteLine("Bank Deductions Summary For:{0} ", bank.BankName);
                Console.WriteLine("Count: {0}", recordcount.ToString());
                Console.WriteLine("Total Amount:{0}", Convert.ToString(Total));


            }
            Console.WriteLine();
            Console.WriteLine("Bank flat files stored in ../BankDebitOrders/BankDebitOrders/bin/Debug");


            Console.Read();

        }
        

       

    }
}
