﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BankDebitOrders
{
   // [XmlRoot("debitorders")]
    [Serializable, XmlRoot("debitorders"), XmlType("debitorders")]
    public class DebitOrders
    {
        [XmlElement("deduction")]
        public List<Deduction> oDeductions { get; set; }

        public DebitOrders()
        {

        }
    }
}
